export interface ShopItem {
  _id: number;
  name: String;
  type: String;
  amount: number;
  price: number;
}
