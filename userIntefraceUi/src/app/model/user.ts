export interface User {
  _id: number;
  firstName: String;
  password: String;
  money: number;
}
