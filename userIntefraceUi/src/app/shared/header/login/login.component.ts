import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../service/user.service';
import {User} from '../../../model/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private user: User;
  private users: User[];

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.getUserByName('Adam');
    this.getUsers();
  }

  private getUserByName(name: String) {
    const userByName = this.userService.getUserByName(name);
    userByName.subscribe(res => {
      this.user = res;
    });
  }

  private getUsers() {
    const users = this.userService.getUsers();
    users.subscribe(res => {
      this.users = res;
    });
  }
}
