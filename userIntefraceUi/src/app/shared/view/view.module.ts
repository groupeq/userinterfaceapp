import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GameListComponent} from './game-list/game-list.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    GameListComponent
  ],
  declarations: [GameListComponent]
})
export class ViewModule {
}
