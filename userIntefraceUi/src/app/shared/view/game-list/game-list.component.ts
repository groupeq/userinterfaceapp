import {Component, OnInit} from '@angular/core';
import {ShopItemService} from '../../../service/shop-item.service';
import {Router} from '@angular/router';
import {ShopItem} from '../../../model/shop-item';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
export class GameListComponent implements OnInit {
  items: ShopItem[];

  constructor(private shopItemService: ShopItemService, private router: Router) {
  }

  ngOnInit() {
    this.createGameList();
  }


  private createGameList() {
    const allItems = this.shopItemService.getAllItems();
    allItems.subscribe(res => {
      this.items = res;
    });
  }

  private sendMeHome() {
    this.router.navigate(['/']);
  }

}
