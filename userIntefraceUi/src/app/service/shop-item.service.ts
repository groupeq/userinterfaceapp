import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {ShopItem} from '../model/shop-item';
import {HttpClient} from '@angular/common/http';

const URL = 'http://localhost:3000';

@Injectable({
  providedIn: 'root'
})
export class ShopItemService {

  private goals = new BehaviorSubject(0);

  goal = this.goals.asObservable();

  constructor(private http: HttpClient) {
  }

  getAllItems(): Observable<ShopItem[]> {
    return this.http.get<ShopItem[]>(URL + '/items');
  }

  getItemByName(name: String): Observable<ShopItem> {
    return this.http.get<ShopItem>(URL + '/items/' + name);
  }
}
