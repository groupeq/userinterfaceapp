import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../model/user';

const URL = 'http://localhost:3000';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  // private user = new BehaviorSubject(0);

  constructor(private httpClient: HttpClient) {
  }

  getUserByName(name: String): Observable<User> {
    console.log(URL + '/users/' + name);
    return this.httpClient.get<User>(URL + '/users/' + name);
  }

  getUsers(): Observable<User[]> {
    return this.httpClient.get<User[]>(URL + '/users');
  }
}
