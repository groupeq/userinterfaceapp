import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FooterComponent} from './shared/footer/footer.component';
import {HeaderComponent} from './shared/header/header.component';
import {MenuComponent} from './shared/menu/menu.component';
import {ViewComponent} from './shared/view/view.component';
import {ShopItemService} from './service/shop-item.service';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {ViewModule} from './shared/view/view.module';
import {HeaderModule} from './shared/header/header.module';

const appRoutes: Routes = [
  {path: '', component: AppComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    MenuComponent,
    ViewComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    ViewModule,
    HeaderModule,
  ],
  providers: [ShopItemService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
