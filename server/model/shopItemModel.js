const shopItemSchema = require('../schema/shopItem');
const mongoose = require("mongoose");

module.exports = mongoose.model('ShopItem', shopItemSchema);
