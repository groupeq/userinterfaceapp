let userSchema = require('../schema/user');
let mongoose = require('mongoose');

module.exports = mongoose.model("UserModel", userSchema);
