module.exports = function (app) {
    let userController = require('../controllers/user-controller');

    app.route('/users')
        .get(userController.getUsers);

    // app.route('/users/:userId')
    //     .get(userController.getUserById);

    app.route('/users/:userName')
        .get(userController.getUserByName);

    app.route('/users/user')
        .post(userController.addUser);
};
