'use strict';
module.exports = function (app) {
    let shopController = require('../controllers/shop-item-controller');
    app.route('/items')
        .get(shopController.getAllItems);

    app.route('/items/:itemId')
        .get(shopController.getItemById)
        .put(shopController.updateItemCapacity);

    app.route('/items/item')
        .post(shopController.addItem);
};

