'use Strict';

let userModel = require("../model/userModel");

exports.getUserById = (req, res) => {
    userModel.findById(req.params.userId, (err, item) => {
        if (err) {
            res.send(err);
        }
        res.json(item);
    })
};

exports.getUsers = (req, res) => {
    userModel.find({}, (err, users) => {
        if (err) {
            res.status(500)
                .send(err)
        }
        res.status(200)
            .send(users)
    })
};

exports.getUserByName = (req, res) => {
    userModel.findOne({firstName: req.params.userName}, (err, user) => {
        if (err) {
            res.send(err);
        }
        res.json(user);
    })
};

exports.addUser = (req, res) => {
    let newUser = new userModel(req.body);
    newUser.save({}, (err) => {
        if (err) {
            res.status(500)
                .send("Could not save user");
        }
        res.status(201)
            .send("Added user to DB: " + newUser.toString());
    })
};
