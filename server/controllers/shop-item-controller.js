'use strict';
let ShopItemModel = require('../model/shopItemModel');

exports.getItemById = (req, res) => {
    console.log(req.params.toString());
    ShopItemModel.findById(req.params.itemId, (err, item) => {
        if (err) {
            res.send("error" + err);
        }
        console.log("Found an item: " + item.toString());
        res.json(item)
    })
};

exports.getAllItems = (req, res) => {
    ShopItemModel.find({}, (err, item) => {
        if (err) {
            res.send(err);
        }
        res.json(item);
    })
};

exports.addItem = (req, res) => {
    let item = new ShopItemModel(req.body);
    item.save({}, (err) => {
        if (err) {
            res.status(500).send(err);
        }
        res.status(201).send("Added element to DB" + item.toString())
    })
};

exports.updateItemCapacity = (req, res) => {
    ShopItemModel.findOneAndUpdate({_id: req.params.itemId}, req.body, {new: true}, (err) => {
        if (err) {
            console.log(err);
        }
        res.status(202).send("Updated element in db");
    });
};
