const properties = require("./resources/properties");
const mongoose = require('mongoose');
const userModel = require('./model/userModel');
const shopItem = require('./model/shopItemModel');
const SERVER_URL = properties.URL;

class Database {

    constructor() {
        this.connect();
    }

    connect() {
        mongoose.connect(SERVER_URL, {'useNewUrlParser': true, 'useCreateIndex': true})
            .then(result => console.log("success" + result))
            .then(Database.createDatabase)
            .catch(err => console.log("error " + err))
    }

    static createDatabase(user, item) {
        user = createUser('Adam', "test", 1500);
        item = createShopItem('Crash', 'Game', 10, 12.32);
        console.log(user.toString());
        console.info(item.toString());

        user.save(err => {
            if (err) {
                console.log("Could not create  " + err);
            } else {
                console.log(user.toString() + " has been added to the DB");
            }
        });
//TODO get rid of these methods outside
        item.save(err => {
            if (err) {
                console.log("Could not create shop item " + err)
            } else {
                console.log(item.toString() + " has been added to the DB")
            }
        });
    }


}

function createUser(name, password, money) {
    return new userModel({
        firstName: name,
        password: password,
        money: money,
    })
}

function createShopItem(name, type, amount, price) {
    return new shopItem({
        name: name,
        type: type,
        amount: amount,
        price: price
    })
}

module.exports = new Database();

