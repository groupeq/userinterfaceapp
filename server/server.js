let express = require('express'), app = express(), port = process.env.PORT || 3000;
let init = require('./databaseInit');
let shopRouter = require('./routes/shop-router');
let userRouter = require('./routes/user-router');
let mongoose = require("mongoose");
let bodyParser = require('body-parser');
const cors = require('cors');
let corsOptions = {
    origin: 'http://localhost:4200',
    optionsSuccessStatus: 200
};


mongoose.set('useFindAndModify', false);
app.listen(port);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors(corsOptions));
shopRouter(app);
userRouter(app);

console.log('Listening on port: ' + port);

